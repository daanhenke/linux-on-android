#!/bin/sh

# Include common functions
source ./shared.sh

# Hardcode container name for now
CONTAINER_NAME="container"

# Hardcode core packages
ARCH_CORE_PACKAGES="filesystem acl archlinuxarm-keyring archlinux-keyring attr bash bzip2 ca-certificates ca-certificates-mozilla ca-certificates-utils coreutils cracklib curl db e2fsprogs expat findutils gcc-libs gdbm glib2 glibc gmp gnupg gnutls gpgme iana-etc keyutils krb5 libarchive libassuan libcap libffi libgcrypt libgpg-error libidn libidn2 libksba libldap libnghttp2 libpsl libsasl libsecret libssh2 systemd-libs libtasn1 libtirpc libunistring libutil-linux linux-api-headers lz4 ncurses nettle npth openssl p11-kit pacman pacman-mirrorlist pam pambase pcre perl pinentry readline shadow sqlite sudo tzdata util-linux which xz zlib zstd"

# Get correct repository
case "$(getarch)" in
    *)
        ARCH_REPO="http://mirror.archlinuxarm.org/aarch64/core";;
esac

# Tell user we found our mirror
msg "Creating new container '$CONTAINER_NAME' from '$ARCH_REPO'"

# Create image
msg "Creating image in ./containers"
mkdir -p containers
busybox dd if=/dev/zero of=containers/"$CONTAINER_NAME".img seek=3999999999 bs=1 count=0

# Format the image
msg "Formatting image..."
busybox mke2fs -F containers/"$CONTAINER_NAME".img

# Mount the image
msg "Mounting image..."
CONTAINER_ROOT=/data/local/containers/"$CONTAINER_NAME"
mkdir -p $CONTAINER_ROOT
busybox mount containers/"$CONTAINER_NAME".img "$CONTAINER_ROOT"

# Create a cache directory
msg "Creating pacman cache '/var/cache/pacman/pkg'"
PACMAN_CACHE="$CONTAINER_ROOT/var/cache/pacman/pkg"
mkdir -p "$PACMAN_CACHE"

# Retrieve package list from repository
msg "Retrieving package list from '$ARCH_REPO'"
ARCH_PACKAGES=$(wget -q -O - "${ARCH_REPO}/" | sed -n '/<a / s/^.*<a [^>]*href="\([^\"]*\)".*$/\1/p' | awk -F'/' '{print $NF}' | sort -rn)

# Install pacman
msg "Installing core packages"
for PACKAGE in ${ARCH_CORE_PACKAGES}
do
    msg "Installing ${PACKAGE}"
    
    PACKAGE_FILE=$(echo "${ARCH_PACKAGES}" | grep -m1 -e "^${PACKAGE}-[[:digit:]].*\.xz$" -e "^${PACKAGE}-[[:digit:]].*\.gz$")

    msg "Downloading ${PACKAGE_FILE}"
    wget -q -c -O "${PACMAN_CACHE}/${PACKAGE_FILE}" "${ARCH_REPO}/${PACKAGE_FILE}"

    msg "Unpacking ${PACKAGE_FILE}"
    case "${PACKAGE_FILE}" in
        *gz) EXTRACT_COMMAND="tar xzf";;
        *xz) EXTRACT_COMMAND="tar xJf";;
    esac
    busybox $EXTRACT_COMMAND ${PACMAN_CACHE}/${PACKAGE_FILE} -C "$CONTAINER_ROOT" --exclude="./dev" --exclude="./sys" --exclude="./proc" --exclude=".INSTALL" --exclude=".MTREE" --exclude=".PKGINFO" > /dev/null 2>&1
done

# Setup nameserver so we can use internet
echo "nameserver 8.8.8.8" > "$CONTAINER_ROOT/etc/resolv.conf"

# Delete the default mtab
rm ${CONTAINER_ROOT}/etc/mtab

# Mount /dev /sys and /proc
busybox mount -o bind /dev ${CONTAINER_ROOT}/dev/
busybox mount -t proc proc ${CONTAINER_ROOT}/proc/
busybox mount -t sysfs sysfs ${CONTAINER_ROOT}/sys/
busybox mount -t devpts devpts ${CONTAINER_ROOT}/dev/pts/

# Create a new mtab
busybox chroot ${CONTAINER_ROOT} ln -s /proc/self/mounts /etc/mtab

# Initialize pacman keyring
busybox chroot ${CONTAINER_ROOT} pacman-key --init

# Populate pacman keyring
busybox chroot ${CONTAINER_ROOT} pacman-key --populate archlinuxarm

# Sync pacman sources
busybox chroot ${CONTAINER_ROOT} pacman -Sy

# Forcefully install base, overwriting the currently installed utilities
busybox chroot ${CONTAINER_ROOT} pacman -S base --force --noconfirm


