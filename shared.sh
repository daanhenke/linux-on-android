# Simple message helper
msg()
{
    echo "> $@"
}

# Get the current architecture
getarch()
{
    local _arch="$(uname -m)"

    case "${_arch}" in
    arm64|aarch64)
        echo "arm64";;
    arm*)
        echo "arm";;
    *)
        echo "arm64";;
    esac
}

# Get extract command by extension
getextractfn()
{
    case "$@" in
        *gz)
            echo "tar xzf";;
        *bz2)
            echo "tar xjf";;
        *xz)
            echo "tar xJf";;
        *)
            echo "false";;
    esac
}